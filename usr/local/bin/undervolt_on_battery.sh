#!/bin/bash
#Undervolt to save energy when on battery (makes sense if turbo mode disabled on battery!)
#Requires tlp (on_ac_power command) and undervolt
#Designed to be triggered by udev and/or systemd events
if /usr/bin/on_ac_power; then
  /usr/local/bin/undervolt --gpu 0 --core 0 --cache 0 --uncore 0
else
  /usr/local/bin/undervolt --gpu -75 --core -100 --cache -100 --uncore -100
fi
