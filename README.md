# undervolt-on-battery

Save battery life by undervolting CPU on Linux when device is disconnected from a power supply.

Designed to be triggered by both udev and/or systemd events.

Tested only with Intel CPU. Together with proper tlp configuration allowed to extend battery life more than twice. Undervolting levels should be safe, but no guarantee at all.

## Prerequisites

Requires tlp (https://github.com/linrunner/TLP) and undervolt (https://github.com/georgewhewell/undervolt).
As undervolting may cause problems when CPU in turbo mode, best if tlp is configured to disable turbo when on battery. This will also save much of battery life.
Included tlp configuration working nice on Asus VivoBook S430 with Intel i5-8265U

## Installation

After prerequisites have been met, just mirror the file structure under `/etc` and `/usr`. Ensure exec permissions for `/usr/local/bin/undervolt_on_battery.sh` script.

### Useful commands

To see driver and power modes, run:
```
sudo cpupower frequency-info
```

If there is no `cpupower` command available install proper packages - on Ubuntu/Mint/Debian this will be:
```
linux-tools-common
linux-tools-$(uname -r)
```
